# Math Game UI

This is the example front end code for the blog post [Spring Boot with Nuxt](https://amydegregorio.com/2021/10/09/spring-boot-with-nuxt/)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

### License

This project is licensed under the Apache License version 2.0.  
See the LICENSE file or go to [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) for more information. 


